package com.example.metronome;

import androidx.appcompat.app.AppCompatActivity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Chronometer;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity{

    private View bar;
    private Button button;
    private Boolean metronomeRunning;
    private Animation rotation1;
    private Animation rotation2;
    private MediaPlayer mp;
    private Timer timer;
    private Chronometer simpleChronometer;
    private long offset = 0;
    //private AnimationDrawable changing_color_animation;

    public void myMethod() {
        mp.start();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button_start);
        metronomeRunning = true;
        button.setText("PLAY");
        button.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick (View v) {
                if(metronomeRunning) {
                    button.setText("STOP");
                    metronomeRunning = false;
                    bar.startAnimation(rotation1);
                    offset = SystemClock.elapsedRealtime();
                    simpleChronometer.setBase(offset);
                    simpleChronometer.start();
                    timer = new Timer();
                    timer.scheduleAtFixedRate(new TimerTask()
                    {
                        @Override
                        public void run()
                        {
                            myMethod();
                        }
                    }, 10, 1000);
                }
                else {
                    button.setText("PLAY");
                    metronomeRunning = true;
                    timer.cancel();
                    bar.startAnimation(rotation1);
                    bar.clearAnimation();
                    simpleChronometer.stop();
                }
            }
        });

        bar = findViewById(R.id.myRectangleView);
        bar.setBackgroundResource(R.drawable.drawable_rectangle_1);

        mp = MediaPlayer.create(this, R.raw.click);
        mp.setLooping(false);

        rotation1 = AnimationUtils.loadAnimation(this, R.anim.left_to_right_animation);
        rotation2 = AnimationUtils.loadAnimation(this, R.anim.right_to_left_animation);

        rotation1.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation rotation1)
            {
                // DO NOTHING
            }

            public void onAnimationRepeat(Animation arg0)
            {
                // DO NOTHING
            }

            public void onAnimationEnd(Animation rotation1)
            {
                bar.startAnimation(rotation2);
                //mp.start();
            }
        });
        rotation2.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationStart(Animation rotation1)
            {
                // DO NOTHING
            }

            public void onAnimationRepeat(Animation arg0)
            {
                // DO NOTHING
            }

            public void onAnimationEnd(Animation rotation2)
            {
                bar.startAnimation(rotation1);
                //mp.start();
            }
        });

        simpleChronometer = findViewById(R.id.simpleChronometer);
    }
}